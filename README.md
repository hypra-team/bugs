Scripts en tous genres
======================

- bug-status: discute avec le BTS pour récupérer les états des différents
bugs, et affiche alors une liste triée par état des bugs.

Normal life of bug:
===================

- bug is reported upstream

- debian bts entry is created with proper forward URL

- Samuel looks:

    bts user bugs@hypra.fr , usertags XXX + samuel-looking

- or Colomban looks:

    bts user bugs@hypra.fr , usertags XXX + colomban-looking

- Perhaps more information is needed to reproduce the bug

    bts user bugs@hypra.fr , usertags XXX + hypra-moreinfo

- The information needed to reproduce the bug was provided

    bts user bugs@hypra.fr , usertags XXX - hypra-moreinfo

- either Samuel works on it:

    bts user bugs@hypra.fr , usertags XXX + samuel

- or Colomban works on it:

    bts user bugs@hypra.fr , usertags XXX + colomban

- or an extern will work on it:

    bts user bugs@hypra.fr , usertags XXX + doextern

- an extern took the task

    bts user bugs@hypra.fr , usertags XXX + extern

- a working patch was created, pending review

    bts tags XXX + patch

- patch was reviewed, but reviewer was not happy with it, so it needs more work

    bts user bugs@hypra.fr , usertags XXX + needwork

- patch was resubmitted

    bts user bugs@hypra.fr , usertags XXX - needwork

- patch was committed upstream (this is normally automatically done by BTS)

    bts tags XXX + fixed-upstream

- patch added to Debian packaging, not uploaded yet

    bts tags XXX + pending

- patch got uploaded to Debian (this is automatically done)


